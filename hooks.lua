function OnPlayerBreakingBlock(Player, block_x, block_y, block_z, block_face, block_type, block_meta)
  -- Check if the player has the permission
  if not (Player:HasPermission('lumberjack.chop')) then
    -- Player does not have the permission
    return false;
  end
  
  -- Check if the player is sneaking
  if (Player:IsCrouched()) then
    -- Player is sneaking
    return false;
  end
  
  -- Check if the player is in survival
  if not (Player:IsGameModeSurvival()) then
    -- Player is not in survival mode
    -- return false;
  end

  -- Check if the blockface is correct
  if (block_face == BLOCK_FACE_NONE) then
    -- Blockface is incorrect
    return false;
  end

  -- Check if the player is using an axe
  if not (ItemCategory.IsAxe(Player:GetEquippedItem().m_ItemType)) then
    -- Player is not using an axe
    return false;
  end

  -- Make sure that the block is wood
  local world = Player:GetWorld();
  local block_type = world:GetBlock(block_x, block_y, block_z);
  if not (ItemCategory.IsWood(block_type)) then
    -- Block is not wood
    return false;
  end

  -- Get the blockmeta
  local block_meta = world:GetBlockMeta(block_x, block_y, block_z);

  -- Initialize the looting class
  local looting = looting_class(Player, block_type, block_meta);

  -- Loop through each block above the current block until the block ID is different
  for pos_y = block_y, world:GetHeight(block_x, block_z), 1 do
    -- Check if the we encounter a different block
    if(world:GetBlock(block_x, pos_y, block_z) ~= block_type) then
      -- Encountered a different block
      break;
    end

    -- Check if we encountered a different blockmeta
    if(world:GetBlockMeta(block_x, pos_y, block_z) ~= block_meta) then
      -- Encountered a different blockmeta
      break;
    end

    -- Remove the block
    world:SetBlock(block_x, pos_y, block_z, E_BLOCK_AIR, 0);

    -- Next block
    looting:NextBlock(block_x, pos_y, block_z);
  end

  -- Finish the looting
  looting:Finish();

  -- Check if we want to replant a sapling
  if (config.ReplantSapling) then
    -- Replant a sapling
    world:SetBlock(block_x, block_y, block_z, E_BLOCK_SAPLING, GetSaplingMeta(block_type, FixWoodMeta(block_meta)));
  end
end