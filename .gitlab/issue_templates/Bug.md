## General information
- Plugin version:
- OS + Arch:
- Cuberite Build:

## Minimal reproduction of the bug/regression with instructions
<!-- If the bug/regression does not include a reproduction, your issue may be closed without resolution. -->


## Expected behaviour
<!-- Describe what the expected behavior would be. -->

## Other information

## If accepted, I would be willing to submit a PR for this feature
[ ] Yes (Assistance is provided if you need help submitting a pull request)  
[ ] No
