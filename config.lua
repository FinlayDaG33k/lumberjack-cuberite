config_defaults = [[
  AxeRequired = true,
  ReplantSapling = true
]];

function LoadDefaultSettings()
  config = loadString("return {" .. config_defaults .. "}")();
end

function LoadSettings()
  -- Get the path to the config
  local config_path = PLUGIN:GetLocalFolder() .. "/config.cfg";

  -- Check if the target path is a file
  if (not cFile:IsFile(config_path)) then
    -- Target path is not a file, load the defaults
    LOG("[" .. PLUGIN:GetName() .. "] The config file doesn't exist. " .. PLUGIN:GetName() .. " will use the default settings for now");
		LoadDefaultSettings();
		return;
  end
  
  -- Get the content from the config
  local config_content = cFile:ReadWholeFile(config_path);

  -- Check if the config is empty
  if (config_content == "") then
    -- Config is empty, load the defaults
    LOG("[" .. PLUGIN:GetName() .. "] The config file is empty. " .. PLUGIN:GetName() .. " will use the default settings for now");
    LoadDefaultSettings();
  end

  -- Load the config
  local config_loader, error = loadstring("return {" .. config_content .. "}");

  -- Check if loading the config went right
  if (not config_loader) then
    -- Something went wrong, load the defaults
    LOG("[" .. PLUGIN:GetName() .. "] Your config seems to be invalid. " .. PLUGIN:GetName() .. " will use the default settings for now");
    LoadDefaultSettings();
  end

  -- Parse the config
  local result, config_table, error = pcall(config_loader);

  -- Check if parsing went right
  if (not result) then
    -- Something went wrong, load the defaults
    LOG("[" .. PLUGIN:GetName() .. "] Your config seems to be invalid. " .. PLUGIN:GetName() .. " will use the default settings for now");
    LoadDefaultSettings();
  end

  -- Save the config
  config = config_table;

  -- Set the looting_class
  looting_class = pickup_looting
end