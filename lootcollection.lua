function looting_skeleton()
  local self = {};
  
	function self:NextBlock()
	end
	
	function self:Finish()
	end
	
	return self;
end

function pickup_looting(Player, block_type, block_meta)
  -- Get the world the player is in
  local world = Player:GetWorld();
  
  -- Create a block collection
  local block = cItems();

  -- Add the block
  block:Add(block_type, 1, FixWoodMeta(block_meta));
  
  -- Create self
	local self = looting_skeleton();
  
  -- Go to the next block
  function self:NextBlock(x, y, z)
    -- Spawn the item
		world:SpawnItemPickups(block, x, y, z, math.random());
	end
	
	return self;
end
