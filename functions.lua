function ItemCategory.IsWood(block_id)
	return (
		(block_id == E_BLOCK_LOG) or
		(block_id == E_BLOCK_NEW_LOG)
	)
end

function GetSaplingMeta(block_type, block_meta)
  -- Check if the block is a log or not
  if ((block_type == E_BLOCK_LOG) or (block_type == E_BLOCK_NEW_LOG)) then
    -- Block is a log, return the meta
		return block_meta;
	end
  
  -- Block is not a log, ignore it
  return block_meta + 4;
end

function FixWoodMeta(block_meta)
  -- Check the block meta to see if it's sideways and use the correct meta
  if (block_meta == 8 or block_meta == 4) then
    -- Oak/Acacia
    block_meta = 0;
  elseif (block_meta == 9 or block_meta == 5) then
    -- Spruce/Dark Oak
    block_meta = 1;
  elseif (block_meta == 10 or block_meta == 6) then
    -- Birch
    block_meta = 2;
  elseif (block_meta == 7 or block_meta == 11) then
    -- Jungle
    block_meta = 3;
  else
    -- No different match, just use the block_meta itself
  end

  return block_meta;
end