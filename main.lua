PLUGIN = nil;
config = nil;
looting_class = nil;

function Initialize(Plugin)
  -- Start initialization
  PLUGIN = Plugin;
  PLUGIN:SetName("Lumberjack");
  PLUGIN:SetVersion(0);
  LOG("[" .. PLUGIN:GetName() .. "] Initializing v." .. PLUGIN:GetVersion());

  -- Load the settings
  LOG("[" .. PLUGIN:GetName() .. "] Loading settings");
  LoadSettings();

  -- Hooks
  LOG("[" .. PLUGIN:GetName() .. "] Enabling hooks");
  cPluginManager:AddHook(cPluginManager.HOOK_PLAYER_BREAKING_BLOCK, OnPlayerBreakingBlock)

  -- Initializing is complete!
  LOG("[" .. PLUGIN:GetName() .. "] Initializing complete!");
  return true;
end

function OnDisable()
  LOG("[" .. PLUGIN:GetName() .. "] Deactivating!");
end